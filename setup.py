from setuptools import setup, find_packages
import os

execfile('glao_psf/_version.py')
long_description = '''
See the README on bitbucket https://bitbucket.org/donald_gavel/glao_psf
'''

if os.environ.get('READTHEDOCS') == 'True':
  requirements = []
else:
  requirements = [
    'matplotlib',
    'six',
    'numpy',
    'scipy',
    'pyfits',
    'h5py',
    'tables',
    'pandas',
    'xmltodict',
  ]

setup(
  name = 'glao_psf',
  packages = find_packages(),
  version = __version__,
  description = 'Adaptive Optics Structure Function and Average PSF calculator',
  long_description = long_description,
  author = 'Don Gavel',
  author_email = 'donald.gavel@gmail.com',
  url = 'https://bitbucket.org/donald_gavel/glao_psf', # the github repo
  install_requires=requirements,
  include_package_data = True,
  download_url = 'https://bitbucket.org/donald_gavel/glao_psf/downloads',
  keywords = ['Adaptive Optics'], # arbitrary keywords
  classifiers = [
    'Development Status :: 5 - Production/Stable',
    'Intended Audience :: Science/Research',
    'License :: OSI Approved :: MIT License',
    'Programming Language :: Python :: 2.7',
    'Topic :: Scientific/Engineering :: Astronomy',
    'Operating System :: MacOS',
    'Operating System :: POSIX :: Linux',
    'Operating System :: Unix',
    ],
)
# 
