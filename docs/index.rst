:tocdepth: 3

.. glao_psf documentation master file, created by
   sphinx-quickstart on Sat May  5 11:47:28 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. include:: ../README.rst

.. toctree::
   :caption: API
   :maxdepth: 6
   :hidden:

   User guide <glao_psf>
   API Documentation <api>
