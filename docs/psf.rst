psf
===

.. currentmodule:: glao_psf.psf

The Point Spread Function generator module.

.. automodule:: glao_psf.psf

.. contents::

Functions
---------

available
^^^^^^^^^

.. autofunction:: available_profiles

fwhm
^^^^

.. autofunction:: fwhm

fwhm2
^^^^^

.. autofunction:: fwhm2

Classes
-------

Cn2_Profile
^^^^^^^^^^^

.. autoclass:: Cn2_Profile
    :members:

Constellation
^^^^^^^^^^^^^

.. autoclass:: Constellation
    :members:


