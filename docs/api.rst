API
===

Programmer's guide - work in progress.

Main modules
------------

.. toctree::
    :maxdepth: 6

    testapi
    psf_fe
    psf

Supporting modules
------------------

.. toctree::
    :maxdepth: 6
    
    info_array
    