info_array
==========

.. currentmodule:: glao_psf.info_array

.. automodule:: info_array

.. contents::

Functions
---------
.. module:: glao_psf.info_array
.. currentmodule:: glao_psf.info_array

load
^^^^

.. autofunction:: load

show
^^^^

.. autofunction:: show

whatsin
^^^^^^^

.. autofunction:: whatsin

Classes
-------

InfoArray
^^^^^^^^^

.. autoclass:: InfoArray
    :members:

InfoMatrix
^^^^^^^^^^

.. autoclass:: InfoMatrix
    :members:
    
-------------------------------

Say something!

