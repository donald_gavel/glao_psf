"""
Version 2 of the glao-psf front end

Rules:
- Read the config file into a dictionary. All values are strings
- Values may have units - this is a space delimited string at the
  end of the value. Strip this off and assign a conversion factor
- Use the format template (config_def) to convert the strings to
  the right type and multiply by the conversion factor
  Values may be lists (comma separated). convert these to lists of
  values with the right type and multiply by the conversion factor
- "follow" is a special rule for the 'radius' key, that ties the radius
  value to the field_point values
- We now have a dictionary with keys and converted values or
  converted lists of values.
  - Use these to create a pandas database of run definitions,
    one run per all combinations of lists
"""

import os
import configparser as cp
import itertools
import pandas as pd
import psf as p
import numpy as np

# key: (section,type,conversion_dictionary,default_value)
config_def = {
    'ngs':('constellation',int,{},3),
    'geom':('constellation',str,{},'circle'),
    'rot':('constellation',float,{'degrees':1},45.),
    'radius':('constellation',float,{'arcmin':1,'arcsec':1./60.},6.),
    'profile':('atmosphere',str,{},'Maunakea 13N median'),
    'r0':('atmosphere',float,{'meters':1.,'cm':0.01},'nan'),
    'outer_scale':('atmosphere',float,{'meters':1., 'm':1.},30.),
    'DM_conjugate':('AO',float,{'meters':1.,'m':1.},-280.),
    'actuator_spacing':('AO',float,{'meters':1.,'cm':0.01},0.),
    'wavelength':('image',float,{'microns':1.,'micron':1,'A':0.1,'angstrom':0.1},0.5),
    'field_point':('image',(float,float),{'arcmin':1.,'arcsec':1./60.},(0.,0.)),
    'filename':('output',str,{},'psf'),
    'seeing_psf_filename':('output',str,{},'psf0'),
    'run_name':('output',str,{},'rdata'),
}

def get_config(configFile=None):
    """
    Get the configuration from a configuration file and
    return a pandas dataFrame defining the PSF runs.
    If the configFile is not specified, use the internal (test) one.
    """
    if configFile is None: return get_configs()
    assert os.path.isfile(configFile), 'file %s not found'%configFile
    config = cp.RawConfigParser(rule0())
    config.read(configFile)
    return parse_config(config)

def get_configs(cstr=None):
    """
    Get the configuration from a string that is formated like a
    config file. Useful for testing against the internal example.
    """
    if cstr is None:
        cstr = test_config_str
    assert isinstance(cstr,(str,unicode))
    if isinstance(cstr,str):
        cstr = unicode(cstr)
    config = cp.RawConfigParser(rule0())
    config.read_string(cstr)
    return parse_config(config)
    
def parse_config(config):
    """
    parse the contents of the config object and return
    a pandas dataFrame of run definitions
    """
    d = {}
    for key in config_def:
        pname = key
        section,typer,units,dval = config_def[key]
        val = config.get(section,pname)
        val,uscale = rule1(val,units)
        val = rule2(key,val,typer,uscale)
        d[key] = val
    df = rule3(d)
    return df

def rule0():
    """create a default config given the config_def
    """
    global default_config
    d = {}
    for key,v in config_def.items():
        d[key] = str(v[3])
    default_config = d
    return d
    
def rule1(val,units):
    """
    strip units and assign conversion factor
    """
    uscale = 1
    unames = units.keys()
    for uname in unames:
        if val.endswith(uname):
            uscale = units[uname]
            val = val[:-len(uname)]
            break
    return val,uscale

def rule2(key,val,typer,uscale):
    """
    convert the string to the type and apply conversion
    """    
    if key == 'radius' and val.startswith('follow'):
        # optional format: 'follow <fac> x <min> [unit]'
        val = val.split(' ')
        fac = float(val[1])
        minr = float(val[3])
        val = {'factor':fac, 'minimum':minr*uscale}
    elif typer is str:
        val = val.split(',')
        if len(val) == 1:
            val = val[0].strip()
        else:
            val = map(lambda x: x.strip(),val)
    elif typer == (float,float):
        val = eval(val)
        try:
            val = map(lambda x: tuple(map(float,x)), val)
        except:
            val = tuple(map(float,val))
    else:
        if typer is int: uscale = 1
        val = val.split(',')
        u = []
        for v in val:
            if v.strip().lower() in ['infinite','infinity']: v = 'inf'
            u.append(v)
        val = map(lambda x: typer(x)*uscale, u)
        if len(val) == 1:
            val = val[0]
    return val

def rule3(d0):
    """
    Convert a dictionary with lists to list of dictionaries,
    iterating over all combinations of the lists.
    Returns a pandas dataFrame, with each row of the dataFrame
    defining the run parameters.
    """
    iters = {}
    for key in d0:
        if isinstance(d0[key],list):
            iters[key] = d0[key]
    param_tuples = list(itertools.product(*iters.values()))
    param_names = iters.keys()
    dset = []
    for ptuple in param_tuples:
        du = dict(zip(param_names,ptuple))
        d = d0.copy()
        d.update(du)
        dset.append(d)
    df = pd.DataFrame(dset)
    return df

def run1(run_def):
    """
    Run the PSF generator for one set of run parameters.
    :param dict run_def: run definition dictionary
    Creates and returns a Cn2_Profile object, which contains
    a Constellation object and all the PSF run results.
    """
    d = run_def
    N = d.ngs
    kwargs = {'L0':d.outer_scale,'lam':d.wavelength,'sf_method':'new'}
    if not pd.isnull(d.r0):
        kwargs['r0'] = d.r0
    
    a = p.Cn2_Profile(d.profile,**kwargs)
    n = 256
    c = p.Constellation(N,d.radius,d.geom,rot=d.rot,field_point = d.field_point)
    r_lim = a.r0*n/8.
    a.make_S(c,r_lim=r_lim,n=n,dm_conjugate=d.DM_conjugate,wfs_conjugate=d.DM_conjugate, act_spacing = d.actuator_spacing)
    w = np.zeros(N+1)
    w[0] = -1
    w[1:] = np.ones(N)/float(N)
    a.make_PSF(w)
    
    speedup = a.PSF_seeing.ena / a.PSF.ena
    a.PSF.speedup = speedup
    
    return a

test_config_str = """
# Keck.cfg - example configuration for Keck GLAO

[constellation]
ngs= 3
geom= circle
radius= 0.5, 1.0, 2.0, 3.0, 5.0, 8.0 arcmin
rot= 45

[atmosphere]
profile= MK2009 50%
outer_scale= 30 meters
#r0= 21 cm

[AO]
dm_conjugate= -126. meters
actuator_spacing= 0.1 meters

[image]
wavelength= 0.5 microns
field_point= (0.,0.)

[output]
run_name= keck_rdata
filename= psf
seeing_psf_filename= psf0
"""

