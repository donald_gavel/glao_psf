'''Assist in loading Matplotlib in
multiple enviromments. Find the appropriate
backend before trying to load it
(and possibly ruining the python session)
including checking for the non-Framework
situation of a virtual environment on a mac.

To use::

    from graphics import plt

More details
------------

In a virtual environment on a mac, python is not installed as a Framework, and
this prevents using ``MacOSX`` as the backend (eny attemt to use it fails).
A work-around is to use the system python /opt/local/bin/python after
setting the environment variable PYTHONHOME=$HOME/[virtual dir].
see: https://blog.rousek.name/2015/11/29/adventure-with-matplotlib-virtualenv-and-macosx/
but this is a cumbersome requirement on the user and easily forgotten.

So, for a mac, we prefer to load TkAgg backend. This requires Tkinter, and
*unfortunately* the virtual environment on the mac is not out-of-the-box configured for Tkinter!
An easy fix is to link to the system tkinter library::

    cd [virtual dir]/lib/python2.7/site-package
    sudo ln -s /opt/local/Library/Frameworks/Python.framework/Versions/2.7/lib/python2.7/site-packages/_tkinter.so .

see: http://lab.openpolis.it/using-tkinter-inside-a-virtualenv.html

As best practice, execute ``from graphics import plt`` as the first command in any python session. This
will indicate right away if there are any graphics back-end
problems and avoid the headache of destroying graphic output after long calculations.
'''

import os
import sys
import time
import platform

machines_without_graphics = ['real.ucolick.org']
machine = platform.node()
preferred_backends = {'-mac-':{'standard':'TkAgg','virtual':'TkAgg'},
                      'darwin':{'standard':'TkAgg','virtual':'TkAgg'},
                      }
#preferred_backends = {} # a test
operating_system = platform.platform()
machine_descriptor = ' '.join([platform.node(),platform.platform()]).lower()
virtual = hasattr(sys,'real_prefix')
framework = 'frameworks' in sys.executable
machine_has_graphics = machine not in machines_without_graphics
plt = None

def epl():
    '''Just-in-time initialization of matplotlib graphics. Use this *instead*
    of ``import matplotlib``, ``matplotlib.use``, ``import matplotlib.pyplot`` etc.
    which can get all fouled up on a mac.
    
    The routine first tries very hard to find a usable graphics backend given the machine,
    possibly virtual environment, mac 'framework', availability of Tkinter,
    etc. In the end, if there is no viable graphic display, it will fall back to a
    PDF file output option.
    
    :return: the :py:mod:`matplotlib.pyplot` module
    
    Example usage::
    
        >>> plt = epl()
        >>> plt.figure()
        >>> plt.plot(...)
    
    Once graphics have been initialized (a one-per-session event), further calls
    will simply return the :py:mod:`matplotlib.pyplot` module.
    '''
    global plt,backend,PdfPages
    
    if plt:
        return plt
    
    if machine_has_graphics:
        # get the preferred agg
        agg = None
        for key,agg in preferred_backends.iteritems():
            if key in machine_descriptor:
                break
            else:
                agg = None
        if not agg:
            print '<graphics> no agg found for %s'%machine_descriptor
            print '<graphics> within preferred_backends: \n %r'%preferred_backends
        else:
            print '<graphics> found preferred backends for %s: agg = %r'%(key,agg)
        print '<graphics> virtual = %r, framework = %r'%(virtual,framework)
        
        import matplotlib
        backend = matplotlib.get_backend()
        print '<graphics> initially, backend = %s'%backend
        if virtual:
            print '<graphics> we think this IS a virtual environment'
            if agg is not None:
                print '<graphics> doing: matplotlib.use(%r)'%agg['virtual']
                matplotlib.use(agg['virtual'])
            else:
                if framework:  # okay, we're using the framework python
                    print "<graphics> we're in a framework, so NOT changing the backend"
                elif backend != 'MacOSX': # it's okay, we're using a backend that doesn't require framework
                    print "<graphics> we're not on a -mac-, no framework needed, so NOT changing backend"
                else:  # in the virtual environment, running non framework python, and MacOSX backend, so...
                    print "<graphics> we're on a -mac-, don't have a framework, and initialized to 'MacOSX' backend, so we need to change the backend"
                    try: # do we have Tkinter?
                        print "<graphics> attempting to determine if we have Tkinter"
                        import Tkinter # try using TkAgg, which will work if we have Tkinter (_tkinter.so in [virtual dir]/lib/python2.7/site-packages)
                        print '<graphics> success: we have Tkinter'
                        print "<graphics> doing matplotlib.use('TkAgg')"
                        matplotlib.use('TkAgg')
                    except: # no Tkinter, so complete fallback to PDF back end
                        print '<graphics> no Tkinter, so necessary to fall back to a non-graphics answer: a PDF backend'
                        print('<graphics> doing: matplotlib.use("PDF")')
                        matplotlib.use('pdf')
        else: # not a virtual environment
            print '<graphics> we think this is NOT a virtual environment'
            if agg is not None:
                print '<graphics> doing: matplotlib.use(%r)'%agg['standard']
                matplotlib.use(agg['std'])
            else:
                print '<graphics> there is no agg, so NOT calling matploblib.use'
                print '<graphics> you may want to edit preferred_backends in graphics.py'
                print '<graphics> backend will remain the same as initial'
            
        backend = matplotlib.get_backend()
        print('<graphics> Final: using %s graphic backend'%backend)
        import matplotlib.pyplot as plt
        from matplotlib.backends.backend_pdf import PdfPages
        plt.ion()
    
    else:
        print('<graphics> WARNING graphics not supported on %s with python %s'%(platform.node(),platform.python_version()))
        plt = None
        backend = None
    
    return plt

def pdf_co(pdfFileName,defaultFileName):
    '''
    PDF conditional open
    
    Enable PDF output for plots, but only if a file name is specified
    or the backend is 'pdf'.
    
    :param str pdfFileName: the name of the pdf file
    :param str defaultFileName: the default name of the file in case pdfFileName is None
    :return: ``pp`` the :class:`PdfPages <matplotlib:matplotlib.backends.backend_pdf.PdfPages>` object, or None
    
    '''
    if not plt:
        _ = epl()

    if backend == 'pdf' or pdfFileName is not None:
        if pdfFileName is None:
            pdfFileName = defaultFileName
        pp = PdfPages(pdfFileName)
        pp.infodict['filename'] = pdfFileName
    else:
        pp = None
    
    return pp

def pdf_cc(pp):
    '''
    PDF conditional close
    
    :param matplotlib.backends.backend_pdf.pdfPages pp: the pdf file writer
    
    close the PDF writer, if not None, and notify if a file was written
    '''
    if pp:
        pp.savefig()
        pp.close()
        print 'Graphs written to\n    %s'%os.path.abspath(pp.infodict['filename'])

