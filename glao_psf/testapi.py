'''
A test of API documents generation on Readthedocs.
'''

class GeneralObject(object):
    '''A generic object
    '''
    def __init__(self):
        pass
    
    def a_method(self):
        '''This is one of the methods
        '''
        pass
    
