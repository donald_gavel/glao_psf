"""PSF generator Front End

The front end acts to translate a run config file, or a run specification dictionary,
into a form that can drive a 'run'. The run involves calling the PSF generator
multiple times and saving each resulting PSF into a FITS file.
A pandas database is returned, and a csv file is written, that summarizes
the results of the run.

"""
import os
import sys
import argparse
import time
import ConfigParser as cp
import psf as p
from graphics import epl, pdf_co, pdf_cc
import numpy as np
import info_array
import warnings
import platform
import itertools
from collections import OrderedDict
import json
import xmltodict
import StringIO
import flog
import pandas as pd
import shutil
from _version import __version__

default_config = {
    'ngs':3,
    'geom':'circle',
    'rot': 45.,
    'radius':6.,
    'profile':'Maunakea 13N median',
    'outer_scale':30.,
    'r0': '',
    'DM_conjugate': -280.,
    'actuator_spacing': 0.,
    'wavelength':0.5,
    'field_point': [(0.,0.), (2.5,0.), (4.,0.), (5.,0.)],
    'run_name':'rdata',
    'filename':'psf',
    'seeing_psf_filename':'psf0',
}

'''
Default values for the run configuration.
'''

example_config_file_contents = '''
[constellation]
ngs= 3
geom= circle
radius= 6.0 arcmin
rot= 45.0 degrees
[atmosphere]
profile= Maunakea 13N median
outer_scale= 30.0 m
[AO]
DM_conjugate= -280.0 m
actuator_spacing= 0.0 meters
[image]
wavelength= 0.5 microns
field_point= (0.0, 0.0), (2.5, 0.0), (4.0, 0.0), (5.0, 0.0) arcmin
[output]
filename= psf
seeing_psf_filename= psf0
run_name= rdata
'''

def run(config=None, save_result=True, verbose=True):
    """ Generate a PSF or set of PSFs using either the config file or a dictionary of args.
    The configuration typically specifies a set of PSF calculation, varying one parameter at
    a time. The results are saved in a sequence of FITS files stored
    in a directory named by the configuration's run_name. A run-summarizing
    spreadsheet "log.csv" is also writen to this directory.
    
    :param str,or,dict config:
        - If :py:obj:`str`, the name of the config file.
        - If :py:obj:`dict`, a dictionary describing the configuration, such as that created by :py:func:`read_config`, which is used instead of a config file

        If config = :py:obj:`None`, then the default config file, distributed with the package, is used.)

    :param bool save_result: whether or not to save the resulting PSF calcuations
        in FITS files. This would only be turned off in, say, a single-case test run
    :param bool verbose: be chatty about it
            
    :return: a :class:`pandas.DataFrame` that summarises the run results.
    
    As a side effect, the last computed result :class:`psf.Cn2_Profile` is stored
    in the global :py:obj:`psf_fe.a`.
    """
    global a
    
    t0 = time.time()
    
    #---------- get run parameters ------------
    if not config:
        config = read_config()
        
    if isinstance(config,(str,unicode)):
        config = read_config(configFile = config)
    
    args = config
    param_tuples = [()]

    largs = default_config
    largs.update(args)
    iters = {}
    for key in largs:
        if isinstance(largs[key],list):
            iters[key] = largs[key]
        else:
            exec('%s = largs["%s"]'%(key,key))
    param_tuples = list(itertools.product(*iters.values()))
    param_names = iters.keys()
    
    valid_profiles = p.available(show=False)
    profiles = args['profile']
    if not isinstance(profiles,list):
        profiles = [profiles]
    for profile in profiles:
        assert profile in valid_profiles, 'profile=%s not valid. profile must be one of %r'%(profile,valid_profiles)
    
    t1 = time.time()
    
    if save_result:
        psf_file_prefix = os.path.splitext(args['filename'])[0]
        seeing_psf_file_prevfix = os.path.splitexxt(args['seeing_psf_filename'])[0]
        run_name = args['run_name']
        ext = 'fits'
        can = Coordinator(run_name,filename,seeing_psf_filename,ext)
    
    k,count = 0,len(param_tuples)
    if not verbose:
        progress_bar(k,count)
        
    if verbose:
        print '(%.3f sec)'%(t1-t0); t0=t1
        
    #---------- do PSF calculation -------
    
        print 'doing PSF calculation...',; sys.stdout.flush()
    
    for param_tuple in param_tuples:  # loop over all the parameters specified as lists
        d = dict(zip(param_names,param_tuple))
        if len(d) > 0:
            if verbose:
                print d
            for key in d:
                exec('%s = d["%s"]'%(key,key))
        if isinstance(args['radius'],dict):  # radius follows field point
            fac,minradius = args['radius'].values()
            radius = np.sqrt(field_point[0]**2 + field_point[1]**2)
            radius = max( radius*fac, minradius)

        N = ngs
        kwargs = {'L0':outer_scale,'lam':wavelength,'sf_method':'new'}
        if r0 != '':
            kwargs['r0'] = r0
        a = p.Cn2_Profile(profile,**kwargs)
        n = 256
        c = p.Constellation(N,radius,'circle',rot=rot,field_point = field_point)
        r_lim = a.r0*n/8.
        a.make_S(c,r_lim=r_lim,n=n,dm_conjugate=DM_conjugate,wfs_conjugate=DM_conjugate, act_spacing = actuator_spacing)
        w = np.zeros(N+1)
        w[0] = -1
        w[1:] = np.ones(N)/float(N)
        a.make_PSF(w)
        
        speedup = a.PSF_seeing.ena / a.PSF.ena
        a.PSF.speedup = speedup

        #------------- save results --------------
        
        a.configFile = configFile
        if save_result:
            can.save(a,verbose=verbose)
        
        k += 1
        if not verbose:
            progress_bar(k,count)
    
    if not verbose:
        print 'done'
    
    if save_result:
        pwd = os.getcwd()
        os.chdir(os.path.join(pwd,can.run_name))
        flog.mkLog(keys='all')
        db = pd.read_csv('log.csv',';')
        os.chdir(pwd)
        shutil.copy(configFile,can.run_name)
    else:
        db = None
        
    t1 = time.time()
    if verbose:
        print '(%.3f sec)'%(t1-t0); t0=t1
    
    return db

class Coordinator(object):
    """A Coordinator object orchestrates the setup, running,
    and storage of results for glao_psf runs.
    It manages a directory of FITS files and its database.
    
    :param str run_name: name of the run
    :param str psf_basename: prefix, before the sequence number, of the PSF FITS file names
    :param str seeing_psf_basename: prefix, befor the sequence number, of the Seeing PSF FITS file names
    :param str ext: extension for the FITS file names (like 'fits', with no dot)
    
    """
    
    def __init__(self,run_name,psf_basename='psf',seeing_psf_basename='psf0',ext='fits'):
        self.run_name = run_name
        ext = ext.strip('.')
        #emsg = "Sorry, a directory named %s already exists. For safety, it won't be overwritten"%run_name
        #assert not os.path.exists(run_name), emsg
        if not os.path.exists(run_name):
            os.mkdir(run_name)
        assert os.path.isdir(run_name), '%s is not a valid directory name (it already exists as a file?)'%run_name
        all_files = os.listdir(run_name)
        n_files = len(all_files)
        filelist = []
        maxind = -1
        tail = '.%s'%ext
        for base in [psf_basename,seeing_psf_basename]:
            head = '%s_'%base
            files = [x for x in all_files if (x.startswith(head) and x.endswith(tail))]
            inds = [x[len(head):-len(tail)] for x in files]
            inds = [int(x) for x in inds]
            if inds:
                maxind = max(max(inds),maxind)
            filelist += files
        self.index = maxind+1
        self.basename = psf_basename
        self.seeing_basename = seeing_psf_basename
        self.filetype = ext
        self._filelist = filelist
        try:
            self.df = pd.read_csv(os.path.join(run_name,'log.csv'),sep=';')
        except:
            if n_files > 0:
                print "<init> There's %d files in this directory, but no log file"%n_files
            self.df = None
    
    @property
    def filelist(self):
        '''Returns a list of the names of the PSF files saved so far.
        '''
        return self._filelist
    
    def save(self,a,verbose=True):
        '''Save one of the PSF results into a FITS file.
        
        :param psf.Cn2_Profile a: the :class:`Cn2_Profile` containing the PSF results
        
        '''
        t0 = time.time()
        
        filename = os.path.join(self.run_name,'%s_%0.4d.fits'%(self.basename,self.index))
        seeing_psf_filename = os.path.join(self.run_name,'%s_%0.4d.fits'%(self.seeing_basename,self.index))
        
        if verbose:
            print 'saving results:'; sys.stdout.flush()
            
        # keys from the profile object
        keys_a = ['name','site','tile','Cn2','Cn2_units','Cn2_bar',
                'databaseFile','databaseMetadata',
                'r0','r0_units','r00','r00_units','spatialFilter',
                'theta0','theta0_units',
                'L0','L0_units',
                'dm_conjugate','dm_conjugate_units',
                'wfs_conjugate','wfs_conjugate_units']
        transkey_a = {'name':'profile','databaseFile':'dbFile','databaseMetadata':'dbMeta'}
        
        # keys from the constellation object
        keys_c = ['N','field_point','geometry','radius','radius_units',
                  'rotation','rotation_units']
        
        # keys from the PSF_seeing object
        keys_p = ['ena','ena_r','ena_units','ena_r_units']
        transkey_p = {'ena':'ena_see','ena_r':'ena_r_se'}
        
        if a.spatialFilter is not False:
            keys_a += ['act_spacing']
        
        a.PSF.configFile = a.configFile
        a.PSF.configFile_units = 'config file for this run'
        a.PSF.version = __version__
        a.PSF.version_units = 'code version'
        
        hdu = a.PSF._hdu()
        hdr = hdu.header

        d = a.PSF_seeing.__dict__
        for key in keys_p:
            if not key.endswith('_units'):
                if key in transkey_p:
                    kwd = transkey_p[key]
                else:
                    kwd = key.upper()[:8]
                val = d[key]
                if key == 'spatialFilter' and not isinstance(val,bool):
                    val = True
                if isinstance(val,(tuple,list,np.ndarray)):
                    val = str(list(val))
                if isinstance(val,dict):
                    val = str(val)
                if isinstance(val,float):
                    val = ['Infinity',val][np.isfinite(val)]
                card = (kwd,val)
                if key+'_units' in keys_p:
                    cmt = d[key+'_units']+' (seeing)'
                    card = card + (cmt,)
                hdr.append(card,end=True)

        hdr.append(('COMMENT','--- Guide Star Constellation ---'),end=True)
        d = a.constellation.__dict__
        for key in keys_c:
            if not key.endswith('_units'):
                kwd = key.upper()[:8]
                val = d[key]
                if isinstance(val,(tuple,list,np.ndarray)):
                    val = str(str(val))
                if isinstance(val,dict):
                    val = str(val)
                if isinstance(val,float):
                    val = ['Infinity',val][np.isfinite(val)]
                card = (kwd,val)
                if key+'_units' in keys_c:
                    cmt = d[key+'_units']
                    card = card + (cmt,)
                hdr.append(card,end=True)
        d = a.__dict__
        hdr.append(('COMMENT','--- Atmosphere characteristics ---'),end=True)
        for key in keys_a:
            if not key.endswith('_units'):
                if key in transkey_a:
                    kwd = transkey_a[key]
                else:
                    kwd = key.upper()[:8]
                val = d[key]
                if key == 'spatialFilter' and not isinstance(val,bool):
                    val = True
                if isinstance(val,(tuple,list,np.ndarray)):
                    val = str(list(val))
                if isinstance(val,dict):
                    val = str(val)
                if isinstance(val,float):
                    val = ['Infinity',val][np.isfinite(val)]
                card = (kwd,val)
                if key+'_units' in keys_a:
                    cmt = d[key+'_units']
                    card = card + (cmt,)
                hdr.append(card,end=True)
            
        if verbose:
            print '    %s'%filename
        with warnings.catch_warnings():
            warnings.filterwarnings('ignore',module='pyfits')
            hdu.writeto(filename,clobber=True)
        
        a.PSF_seeing.configFile = a.configFile
        a.PSF_seeing.configFile_units = 'config file for this run'
        a.PSF_seeing.version = __version__
        a.PSF_seeing.version_units = 'code version'

        a.PSF_seeing.profile = a.profile
        a.PSF_seeing.Cn2 = str(list(a.Cn2))
        a.PSF_seeing.L0 = ['Infinity',a.L0][np.isfinite(a.L0)]
        a.PSF_seeing.L0_units = a.L0_units
        hdu = a.PSF_seeing._hdu()
        if verbose:
            print '    %s'%seeing_psf_filename
        with warnings.catch_warnings():
            warnings.filterwarnings('ignore',module='pyfits')
            hdu.writeto(seeing_psf_filename,clobber=True)
        
        self.index += 1
        self._filelist.append((filename,seeing_psf_filename))
        
        t1 = time.time()
        if verbose:
            print '(%.3f sec)'%(t1-t0); t0=t1
            print 'done'; sys.stdout.flush()

def summary(a=None,pdfFile=None):
    '''Print and plot results from a PSF calculation.
    
    :param psf.Cn2_Profile a: the result from a PSF calculation run.  See :py:func:`run`.
    
    If ``a`` is :py:obj:`None` then use the global :py:class:`psf.Cn2_Profile` object :py:obj:`psf_fe.a` that
    resulted from the most recent PSF calculation.
    '''
    if a is None:
        a = globals()['a']
        
    print 'GLAO PSF ENA = %0.4f %s'%(a.PSF.ena,a.PSF.ena_units)
    print 'Seeing PSF ENA = %0.4f %s'%(a.PSF_seeing.ena,a.PSF_seeing.ena_units)
    print 'GLAO PSF ENA_r = %0.4f %s'%(a.PSF.ena_r,'arcsec')
    print 'Seeing PSF ENA_r = %0.4f %s'%(a.PSF_seeing.ena_r,'arcsec')
    
    plt = epl()
    pp = pdf_co(pdfFile,'glao_psf_summary.pdf')
    
    plt.figure()
    a.constellation.graph()
    plt.grid('off')
    if pp:
        pp.savefig()
    a.PSF.graph(label='GLAO')
    a.PSF_seeing.graph(oplot=True,label='seeing')
    plt.legend()
    if pp:
        pp.savefig()
    a.PSF.show()
    if pp:
        pp.savefig()
    a.PSF_seeing.show()

    pdf_cc(pp)

config_syntax = OrderedDict((
    ('constellation/ngs',[int,{}]),
    ('constellation/geom',[str,{}]),
    ('constellation/radius',[float,{'arcmin':1.,'arcsec':1./60.}]),
    ('constellation/rot',[float,{'degrees':1}]),
    ('atmosphere/profile',[str,{}]),
    ('atmosphere/outer_scale',[float,{'meters':1., 'm':1.}]),
    ('atmosphere/r0',[float,{'meters':1.,'cm':0.01}]),
    ('AO/DM_conjugate',[float,{'meters':1.,'m':1.}]),
    ('AO/actuator_spacing',[float,{'meters':1.,'cm':0.01}]),
    ('image/wavelength',[float,{'microns':1.,'micron':1.,'A':0.1,'angstrom':0.1}]),
    ('image/field_point',[(float,float),{'arcmin':1.,'arcsec':1./60.}]),
    ('output/filename',[str,{}]),
    ('output/seeing_psf_filename',[str,{}]),
    ('output/run_name',[str,{}]),
))
    
def write_config(d,configFile=None,format='native',clobber=False):
    '''Write a config file given the dictionary of configs
    
    :param dict d: dictionary with configuration parameters
    :param str configFile: the config file name to write to. If :py:obj:`None` then return a string
        of text that would have been written to the file
    :param str format: file format ('xml', 'json', or 'native' where 'native' is the
        format described in the :ref:`user docs <glao_psf>`)
    :param bool clobber: whether or not to over write an existing file. The
        default is :py:obj:`False` for safety
    
    If **configFile** is :py:obj:`None` then return the string that would
    have been written to the file
    '''
    if configFile:
        if not clobber:
            assert not os.path.exists(configFile)
        f = open(configFile,'w')
    else:
        f = StringIO.StringIO()
    
    if format == 'native':
        category = ''
        for key,(_,unitd) in config_syntax.iteritems():
            cat,param = key.split('/')
            if param in d:
                val = d[param]
                if val == '':
                    continue
                if isinstance(val,str) and val.lower() in ['inf','infinity']:
                    val = float('inf')
                if category != cat:
                    category = cat
                    f.write('[%s]\n'%category)
                if isinstance(val,list):
                    val = str(val).strip('[]')
                line = '%s= %s'%(param, str(val))
                for unitname,unitval in unitd.iteritems():
                    if unitval == 1:
                        line += ' %s'%unitname
                        break
                
                f.write(line+'\n')
        
    elif format == 'json':
        json.dump(d,f,indent=4,separators=(',',':'))
        
    elif format == 'xml':
        dd = {}
        for key,(typer,udict) in config_syntax.iteritems():
            namespace,key = key.split('/')
            typer = str(typer).split("'")[1]
            if key in d:
                val = d[key]
                dd[key] = {'@type':typer,'value':val }
                unit_name = None
                for unit_name,unit_scale in udict.iteritems():
                    if unit_scale == 1.:
                        dd[key]['@unit'] = unit_name

        xmltodict.unparse({'root':dd},output=f,pretty=True)
    
    if not configFile:
        s = f.getvalue()
    else:
        s = None
    
    f.close()
    return s

def read_config(configFile='example.cfg', format='native',verbose=True):
    """Read the config file and generate a dictionary that can be sent
    as an argument to :py:func:`run`
    
    :param str configFile: the configuration file name
    :param str format: file format ('xml', 'json', or 'native' where 'native' is the
        format described in the :ref:`user docs <glao_psf>`)
    :param bool verbose: be chatty about it
    
    The configFile can also be a file-like object.
    To send a string as if it were a file, use :class:`StringIO`::
    
        >>> d = read_config(StringIO(s),...)
    
    """
    formats = ['native','json','xml']
    c1_set = ['[','{','<']
    format_dict = dict(zip(c1_set,formats))

    args = {}
    args.update(default_config)
    
    # if the argument is the name of a config file make sure it exists
    
    if isinstance(configFile,(str,unicode)):
        args['configFile'] = configFile
        if verbose:
            print 'reading config params from %s'%configFile
        configFile = os.path.expanduser(configFile)
        if not os.path.isfile(configFile):
            if configFile == 'example.cfg':
                path = os.path.abspath(__file__)
                dir_path = os.path.dirname(path)
                exampleConfigFile = os.path.join(dir_path,'example.cfg')
                configFile = exampleConfigFile
                print '### using example config file from code source directory ###'
                print '    %s'%os.path.abspath(configFile)
            else:
                raise IOError,'no file named %s'%configFile
        f = open(configFile,'r')
    else:
        args['configFile'] = str(f)
        f = configFile

    if format == 'native':
            
        config = cp.ConfigParser()
        config.readfp(f)
        
        for key in config_syntax:
            section,pname = key.split('/')
            try:
                val = config.get(section,pname)
            except:
                continue
            typer,units = config_syntax[key]
            unames = units.keys()
            uscale = 1
            for uname in unames:
                if val.endswith(uname):
                    uscale = units[uname]
                    val = val[:-len(uname)]
                    break
            if len(val) == 0:
                val = ''
            else:
                if pname == 'radius' and val.startswith('follow'):
                    # optional format: 'follow <fac> x <min> [unit]'
                    val = val.split(' ')
                    fac = float(val[1])
                    minr = float(val[3])
                    val = {'factor':fac, 'minimum':minr*uscale}
                elif typer is str:
                    val = val.split(',')
                    if len(val) == 1:
                        val = val[0].strip()
                    else:
                        val = map(lambda x: x.strip(),val)
                elif typer == (float,float):
                    val = eval(val)
                    try:
                        val = map(lambda x: tuple(map(float,x)), val)
                    except:
                        val = tuple(map(float,val))
                else:
                    val = val.split(',')
                    u = []
                    for v in val:
                        if v.strip().lower() in ['infinite','infinity']: v = 'inf'
                        u.append(v)
                    val = map(lambda x: typer(x)*uscale, u)
                    if len(val) == 1:
                        val = val[0]
            args[pname] = val

    elif format == 'json':
        args.update( json.load(f) )
    
    elif format == 'xml':
        d = xmltodict.parse(f)['root']
        
        for key,(typer,udict) in config_syntax.iteritems():
            namespace,key = key.split('/')
            if key in d:
                if '@unit' in d[key]:
                    unit_factor = d[key]['@unit']
                else:
                    unit_factor = 1
                val = d[key]['value']
                if val is None:
                    val = ''
                    typer = str
                if isinstance(val,list):
                    try:
                        val = [eval(x) for x in val]
                    except:
                        raise Exception,'error evaluating %r: %r'%(key,val)
                elif typer != str:
                    try:
                        val = eval(val)
                    except:
                        raise Exception,'error evaluating %r: %r'%(key,val)
                args[key] = val
            
    else:
        raise Exception,'%s format not supported'%format
    
    f.close()
    return args

def progress_bar(n,N):
    """This creates and maintains a progress bar to show the status of long computations
    
    :param int n: the number of operations performed so far
    :param int N: the total number of operations to be performed
    """
    pc = int(np.round(100*float(n)/float(N)))
    N_max = 50
    d = N//N_max + 1
    N = N//d
    n = n//d
    sys.stdout.write('\r')
    fmt = '[%%-%ds]' % N
    ostr = (fmt+' %d%%') % ('='*n,pc)
    sys.stdout.write(ostr)
    sys.stdout.flush()

def test():
    '''Test run (simple)
    '''
    d = read_config('keck.cfg')
    d['radius'] = 5.
    run(config=d,verbose=False,save_result=False)

def test_many():
    '''Test run based completely on a configuration file
    '''
    d = read_config('keck.cfg')
    run(config=d,save_result=True)
    
def test_manyL0():
    '''Test run of multiple PSF calculations
    '''
    d = read_config('keck.cfg')
    d['radius'] = 5.
    d['DM_conjugate'] = 0.
    L0_set = [20.,30.,50.,100.,float('Infinity')]
    for L0 in L0_set:
        d['outer_scale'] = L0
        run(config=d,verbose=False,save_result=False)
        print '<test> L0 = %0.3f, ENA_r = %0.3f (%0.3f)'%(a.L0,a.PSF.ena_r,a.PSF_seeing.ena_r)
        
if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--verbose',nargs='?',default=False,const=True,type=bool,help='print diagnostics as it runs')
    parser.add_argument('--summary',nargs='?',default=False,const=True,type=bool,help='create a summary and save graphs to a file')
    parser.add_argument('config_file',nargs='?',default='example.cfg',type=str,help='config file name')
    args = parser.parse_args()
    configFile = args.config_file
    verbose = args.verbose
    run(configFile,verbose=verbose)
    if args.summary:
        summary(pdf=True)
